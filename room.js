"use strict";

var http = require('http');
var parseString = require('xml2js').parseString;

var url = "http://herofighter.com/hf/rlg.php?ver=700&cc=de&s="

class Room {
	constructor (t, ip, pt, rn, dc, n, nl, cc, ppl) {
		this.t = t;
		this.ip = ip;
		this.pt = pt;
		this.rn = rn;
		this.dc = dc;
		this.n = n;
		this.nl = nl;
		this.cc = cc;
		this.ppl = ppl;
	}
}

function getAllRooms(callback) {
	var arr = [];
	http.get(url + Math.floor(Math.random() * 99999), (resp) => {
		let data = '';

		// A chunk of data has been recieved.
		resp.on('data', (chunk) => {
			data += chunk;
		});

		// The whole response has been received. Print out the result.
		resp.on('end', () => {
			parseString(data, function (err, result) {
				for (var k in result['rl']['room']){
					if (result['rl']['room'].hasOwnProperty(k)) {
						//console.dir( result['rl']['room'][k]);
						var t = result['rl']['room'][k]['t'][0];
						var ip = result['rl']['room'][k]['ip'][0];
						var pt = result['rl']['room'][k]['pt'][0];
						var rn = result['rl']['room'][k]['rn'][0];
						var dc = result['rl']['room'][k]['dc'][0];
						var n = result['rl']['room'][k]['n'][0];
						var nl = result['rl']['room'][k]['nl'][0];
						var cc = result['rl']['room'][k]['cc'][0];
						var ppl = result['rl']['room'][k]['ppl'][0];
						arr.push(new Room(t, ip, pt, rn, dc, n, nl, cc, ppl));
					}
				}
				callback(arr);
			});
		});

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});
}

module.exports.Room = Room;
module.exports.getAllRooms = getAllRooms;
