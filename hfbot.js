"use strict";

/**
 * Global Variables
 */
var ip = '94.62.120.226';//'188.68.40.186'//'94.62.120.226'
var port = 8888;//63000;//8888;
var nickname = 'oblivion';
var clientID = 0;

/**
 * Imports...
 */
var cleverbot = require("cleverbot.io");
var net = require('net');
var priv = require("./private");
var RS = require("./room");

/**
 * Initialize cleverbot
 */
var bot = new cleverbot(priv.api_user, priv.api_key);
//bot.setNick("hfbot");
bot.create(function (err, session) {
	//Can have code here
});

function joinEmptyRoom() {
	RS.getAllRooms(function(rooms) {
		var i;
		for (i = 0; i < rooms.length; i++) {
			if (rooms[i].n === '0' && rooms[i].dc.includes('\u221e')) {
				connectToRoom(rooms[i].ip, rooms[i].pt);
				break;
			}
		}
		// If all rooms full, try again in 1 minute
		if (i === rooms.length) {
			setTimeout(function() {
				joinEmptyRoom();
			}, 60*1000); // 1 minute
		}
	});
}

joinEmptyRoom();

/**
 * Connect to server
 */
function connectToRoom(ip, port) {
	var client = new net.Socket();
	var players = [];
	var data_tmp = '';

	client.connect(port, ip, function() {
		chatLog('Connected\nIP: ' + ip + '\nPort: ' + port);
		client.setEncoding('utf8');
		players = [];
	});

	/* Receives chunks of data but does not stop at newline */
	client.on('data', function(data) {
		//data = data.toString("utf8"); // In case utf8 encoding is not set

		data = data_tmp + data;
		if(!data.includes('\n')) { data_tmp += data; return; }
		var lines = data.split(/\r?\n/);
		if(data[data.length-1] !== '\n') {
			// If last line is incomplete, save it and remove it from current lines array.
			data_tmp = lines[lines.length-1];
			lines.splice(-1,1);
		}
		else {
			// If last line is complete, the split function created an empty line at the end, remove it.
			data_tmp = '';
			lines.splice(-1,1);
		}
		//console.dir(lines);
		for (var i = 0, len = lines.length; i < len ; i++) {
			// Ping replies
			if(lines[i].includes('<ping></ping>')) {
				client.write('<pingok></pingok>\n');
			}
			// Join room
			else if(lines[i].includes('<ini></ini>')) {
				client.write('<join><ver>700</ver></join>\n');
			}
			// If room requires password, quit program
			else if(lines[i].includes('<req_pw></req_pw>')) {
				chatLog('Room requires password.');
				client.destroy(); // kill client after server's response
				process.exit();
			}
			// Give information about this fake client
			else if(lines[i].includes('<req_info></req_info>')) {
				client.write('<info><name><![CDATA[' + nickname + ']]></name><cc>PT</cc><p><![CDATA[' + nickname + ' P1]]></p><p><![CDATA[' + nickname + ' P2]]></p><p><![CDATA[' + nickname + ' P3]]></p><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>0</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>16</l><l>0</l><l>0</l><l>0</l><l>0</l><l>0</l><l>0</l><l>0</l><l>0</l><l>0</l><l>0</l></info>\n');
			}
			// If error joining room, quit program
			else if(lines[i].includes('<error><reason>')) {
				chatLog('Got error trying to join.');
				chatLog(lines[i]);
				client.destroy(); // kill client after server's response
				process.exit();
			}
			// Use cleverbot to reply to chat messages
			else if(lines[i].includes('<chat><c>') && !lines[i].includes('<font color=')
			        && !lines[i].includes('<c>' + clientID + '</c>')) {
				var cid = lines[i].substring(lines[i].lastIndexOf("<c>")+3, lines[i].lastIndexOf("</c>"));
				var msg = lines[i].substring(lines[i].lastIndexOf("<m><![CDATA[")+12, lines[i].lastIndexOf("]]></m>"));
				chatLog(players[cid][cid] + ' says: ' + msg);
				bot.ask(msg[1], function (err, response) {
					writeToChat(client, response);
					chatLog(nickname + ' says: ' + response);
				});
			}
			// Get client number so that replies are sent from this nickname
			else if(lines[i].includes('<list><cid>')) {
				var cid = lines[i].substring(lines[i].indexOf("<list><cid>") + 11, lines[i].indexOf("</cid>"));
				clientID = cid;
				players = parsePlayers(lines[i]);
				var playerCount = onlinePlayerCount(lines[i]);
				// If there is more than one real player in the room, join another room with no one.
				if(playerCount > 2) {
					writeToChat(client, 'Bye bye.');
					client.destroy();
					return;
				}
			}
			// Change page so game doesn't hang
			else if(lines[i].includes('<page>') && lines[i].includes('<prepage>')) {
				var page = lines[i].substring(lines[i].lastIndexOf("<page>"), lines[i].lastIndexOf("</page>") + 7);
				client.write(page + '\n');
				if(page.includes('FIGHTING')) {
					client.write('<startedgame>FIGHTING</startedgame>\n');
				}
			}
			else if(lines[i].includes('<startcount><action>restartgame</action><count>0</count><page>FIGHTING</page></startcount>')) {
				client.write('<page>FIGHTING</page>\n<startedgame>FIGHTING</startedgame>\n');
			}

			//chatLog('LINE: ' + lines[i] + '\n\n');
		}

	});

	client.on('close', function() {
		chatLog('Connection closed');
		joinEmptyRoom();
	});
}

function chatLog(msg) {
	console.log('[' + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + '] ' + msg);
}

function writeToChat(client, msg) {
	client.write('<chat><c>' + clientID + '</c><m><![CDATA[' + msg + ']]></m></chat>\n');
}

function parsePlayers(data) {
	var players = [];
	var name;
	for (var i = 0; i < 4; i++) {
		var name = data.substring(data.indexOf("<info><name><![CDATA[") + 21, data.indexOf("]]></name>"));
		data = data.substring(data.indexOf("]]></name>") + 10);
		players.push({[i]: name});
	}
	return players;
}

function onlinePlayerCount(data) {
	var n = (data.match(/\<online\>1\<\/online\>/g)||[]).length;
	return n;
}
