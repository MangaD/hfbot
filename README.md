Install Node.js
```sh
sudo apt-get install nodejs
```

Install the Node package manager, npm:
```sh
sudo apt-get install npm
```

Build package.json
```sh
npm init
```

Cleverbot
```sh
npm install --save cleverbot.io
```

XML Parser
```sh
npm install --save xml2js
```
